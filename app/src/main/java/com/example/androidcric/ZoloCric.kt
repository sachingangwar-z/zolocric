package com.example.androidcric

import android.os.Parcel
import android.os.Parcelable

data class ZoloCric(val image:Int, val name: String, val rating : Float,val desc: String): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readFloat(),
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(image)
        parcel.writeString(name)
        parcel.writeFloat(rating)
        parcel.writeString(desc)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ZoloCric> {
        override fun createFromParcel(parcel: Parcel): ZoloCric {
            return ZoloCric(parcel)
        }

        override fun newArray(size: Int): Array<ZoloCric?> {
            return arrayOfNulls(size)
        }
    }
}