package com.example.androidcric

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class DescriptionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_description)

        val cricketer=intent.getParcelableExtra<ZoloCric>("cricketer")
        if(cricketer!=null){
            val textView:TextView=findViewById(R.id.descriptionActivityTV)
            val imageView: ImageView=findViewById(R.id.descriptionActivityIV)
            val ratingBar: RatingBar=findViewById(R.id.ratingBar)
            var desc: TextView =findViewById(R.id.textView2)

            textView.text=cricketer.name
            imageView.setImageResource(cricketer.image)
            ratingBar.rating = cricketer.rating
            desc.text=cricketer.desc
        }
    }
    }