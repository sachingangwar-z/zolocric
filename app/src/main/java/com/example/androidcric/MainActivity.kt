package com.example.androidcric

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var cricketerList: ArrayList<ZoloCric>
    private lateinit var zoloCricAdapter:ZoloCricAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView=findViewById(R.id.recyclerView)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager=LinearLayoutManager(this)

        cricketerList= ArrayList()
        cricketerList.add(ZoloCric(R.drawable.abd,"AB De Villiers",4.1f,"Born  Feb 17, 1984 (38 years)\n"+"Birth Place  Pretoria\n" + "Height  5 ft 10 in (178 cm)\n"+ "Role  WK-Batsman\n" +"Batting Style  Right Handed Bat\n"+ "Bowling Style  Right-arm medium"))
        cricketerList.add(ZoloCric(R.drawable.bumrah,"Jasprit Bumrah",3.1f,
                "Born  Feb 17, 1984 (38 years)\n"+"Birth Place  Pretoria\n" + "Height  5 ft 10 in (178 cm)\n"+ "Role  WK-Batsman\n" +"Batting Style  Right Handed Bat\n"+ "Bowling Style  Right-arm medium"))
        cricketerList.add(ZoloCric(R.drawable.msd,"MS Dhoni",5.0f,
                "Born  Feb 17, 1984 (38 years)\n"+"Birth Place  Pretoria\n" + "Height  5 ft 10 in (178 cm)\n"+ "Role  WK-Batsman\n" +"Batting Style  Right Handed Bat\n"+ "Bowling Style  Right-arm medium"))
        cricketerList.add(ZoloCric(R.drawable.hardik,"Hardik Pandya",3.5f,
                "Born  Feb 17, 1984 (38 years)\n"+"Birth Place  Pretoria\n" + "Height  5 ft 10 in (178 cm)\n"+ "Role  WK-Batsman\n" +"Batting Style  Right Handed Bat\n"+ "Bowling Style  Right-arm medium"))
        cricketerList.add(ZoloCric(R.drawable.mccullum,"Brendon Mccullum",3.5f,
                "Born  Feb 17, 1984 (38 years)\n"+"Birth Place  Pretoria\n" + "Height  5 ft 10 in (178 cm)\n"+ "Role  WK-Batsman\n" +"Batting Style  Right Handed Bat\n"+ "Bowling Style  Right-arm medium"))
        cricketerList.add(ZoloCric(R.drawable.rohit,"Rohit Sharma",3.5f,
                "Born  Feb 17, 1984 (38 years)\n"+"Birth Place  Pretoria\n" + "Height  5 ft 10 in (178 cm)\n"+ "Role  WK-Batsman\n" +"Batting Style  Right Handed Bat\n"+ "Bowling Style  Right-arm medium"))
        cricketerList.add(ZoloCric(R.drawable.smith,"Steven Smith",4.1f,
                "Born  Feb 17, 1984 (38 years)\n"+"Birth Place  Pretoria\n" + "Height  5 ft 10 in (178 cm)\n"+ "Role  WK-Batsman\n" +"Batting Style  Right Handed Bat\n"+ "Bowling Style  Right-arm medium"))
        cricketerList.add(ZoloCric(R.drawable.virat,"Virat Kohli",4.5f,
                "Born  Feb 17, 1984 (38 years)\n"+"Birth Place  Pretoria\n" + "Height  5 ft 10 in (178 cm)\n"+ "Role  WK-Batsman\n" +"Batting Style  Right Handed Bat\n"+ "Bowling Style  Right-arm medium"))

        zoloCricAdapter= ZoloCricAdapter(cricketerList)
        recyclerView.adapter=zoloCricAdapter

        zoloCricAdapter.onItemClick={
            val intent=Intent(this,DescriptionActivity::class.java)
            intent.putExtra("cricketer",it)
            startActivity(intent)
        }
    }
}