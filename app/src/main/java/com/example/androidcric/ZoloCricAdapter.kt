package com.example.androidcric

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ZoloCricAdapter(private val cricketerList:ArrayList<ZoloCric>):
    RecyclerView.Adapter<ZoloCricAdapter.ViewHolder>() {

    var onItemClick: ((ZoloCric)->Unit)?=null

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val imageView: ImageView=itemView.findViewById(R.id.imageView)
        val textView: TextView=itemView.findViewById(R.id.textView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.activity_cricketer,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return cricketerList.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cricketer=cricketerList[position]
        holder.imageView.setImageResource(cricketer.image)
        holder.textView.text=cricketer.name
        holder.itemView.setOnClickListener{
            onItemClick?.invoke(cricketer)
        }
    }
}